// Datatable
let tbody = document.querySelector('.table-body')
const itemsPerPage  = document.querySelector('.items-per-page')
const pageNumberButton = document.querySelector('.pagination')
const paginationButtons = document.querySelectorAll('.pagination-button')
const btnPrev = document.querySelector('.btn-prev')
const btnNext = document.querySelector('.btn-next')
const infor = document.querySelector('.infor-text')
const inputSearch = document.querySelector('.input-search')
const infoFilter = document.querySelector('.info-filter')
const sorts = document.querySelectorAll('.sort')
let sortBy = "name"
let order = "asc"
let toggle = false
let activeSortId = "name"
let currentPage = 1
let limit = itemsPerPage.value
let listCurrent = []
let list = []
const dataAPI = 'https://64547b11a74f994b334172b5.mockapi.io/employee'


// get data from api
function getData(callback){
    fetch(dataAPI)
    .then(function (response) {
        return response.json()
    })
    .then(function (response) {      
        list = response
        listCurrent = list
        return response
    })
    .then(callback)
}

function renderdata(employee, index) {
    let bg = ""
    let boxshadow = ""
    index % 2 === 0 ? bg = "old" : bg = "even" 
    return `<tr class="${bg}">
        <td>${employee.name}</td>
        <td>${employee.position}</td>
        <td>${employee.office}</td>
        <td>${employee.age}</td>
        <td>${employee.startDate}</td>
        <td>$${employee.salary.toLocaleString()}</td>
    </tr>`
}

function showItemReload(){
    sort()
    loadItem()
    renderPaginationButtons()
    infor.innerHTML = changeDataTableInfo()
}

function loadItem(){
    let htmls = ""
    let begin = limit * (currentPage - 1)
    let end = limit * currentPage - 1
    listCurrent.forEach((item , index) => {
        if(index >= begin && index <= end){
            htmls += renderdata(item, index)
        }
    })
    tbody.innerHTML = htmls
}

function renderPaginationButtons(){
    let htmlBtns = ""
    const sumPage = Math.ceil(listCurrent.length / limit)
    for(let i = 1;i <= sumPage; i++){
        if(i == currentPage) {
            htmlBtns += `<button class="btn btn-hover btn-selected" id=${i} onclick="handleClick(event.target,${i})">${i}</button>`
        } 
        else  htmlBtns += `<button class="btn btn-hover" id=${i} onclick="handleClick(event.target,${i})">${i}</button>`
    }
    pageNumberButton.innerHTML = htmlBtns
}

function changePage(i){
    currentPage = i
    loadItem()
}

function changeStatusButton(button){
    const btnActive = document.querySelector('.btn.btn-selected')
    if(btnActive){
        btnActive.classList.remove('btn-selected')
        btnActive.classList.add('btn-hover')
    }
    button.classList.remove('btn-hover')
    button.classList.add('btn-selected')
}

function changeDataTableInfo(){
    let begin = limit * (currentPage - 1)
    let end = limit * currentPage - 1
    return `
        Showing ${begin + 1} to ${end + 1 < listCurrent.length ? end + 1 : listCurrent.length} of 36 entries
    `
}

function disableButtonPrev() {
    if(currentPage < 2){
        btnPrev.disabled = true
        btnPrev.classList.remove('btn-hover')
    } else {
        btnPrev.disabled = false
        btnPrev.classList.add('btn-hover')
    }
}

function disableButtonNext() {
    const number = Math.ceil(listCurrent.length / limit)
    if(currentPage > number - 1){
        btnNext.disabled = true
        btnNext.classList.remove('btn-hover')
    } else {
        btnNext.disabled = false
        btnNext.classList.add('btn-hover')
    }
}


// Search function
function search(valueinput){
    return list.filter(item => {
        for(let x of Object.values(item)){
            if(typeof(x) === 'string'){
                if(x.toLowerCase().includes(valueinput.toLowerCase())) return item
            } else {
                if(x == valueinput) return item
            }
        }
    })
}

// Sort function
function sort() {
    const listSort = listCurrent.sort((a, b) => {
      if (a[sortBy] < b[sortBy]) {
        return order === "asc" ? -1 : 1;
      }
      if (a[sortBy] > b[sortBy]) {
        return order === "asc" ? 1 : -1;
      }
      return 0;
    });
    listCurrent = listSort;
}

// event click
function handleClick(button, i){
    changePage(i)
    changeStatusButton(button)
    disableButtonPrev()
    disableButtonNext()
    infor.innerHTML = changeDataTableInfo()
}

// event select
itemsPerPage.addEventListener('change', (e) => {
    limit = e.target.value
    currentPage = 1
    showItemReload()
    disableButtonPrev()
    disableButtonNext()
})


// event input 
inputSearch.addEventListener('input', () => {
    if(inputSearch.value == ""){
        infoFilter.innerHTML = ""
        listCurrent = list
    }
    else {
        infoFilter.innerHTML = "(filtered from 36 total entries)"
        listCurrent = search(inputSearch.value)
    }
    showItemReload()
})


// event prev/next
paginationButtons.forEach(paginationButton => {
    paginationButton.addEventListener('click', (e) => {
        if(e.target.id === 'prev') {
            currentPage--
        } else {
            currentPage++
        }
        disableButtonNext(e.target)
        disableButtonPrev(e.target)
        changePage(currentPage)
        let listBtn = document.querySelectorAll('.btn')    
        listBtn = Array.from(listBtn).slice(1, listBtn.length - 1)
        changeStatusButton(listBtn[currentPage - 1])
        
    })
})


// event sort
sorts.forEach(sort => {
    sort.addEventListener('click', (e) => {
        if(activeSortId !== e.target.id) toggle = false
        activeSortId = e.target.id
        sortBy = e.target.id
        const sortingASC = document.querySelector('.sort.sorting_asc')
        const sortingDESC = document.querySelector('.sort.sorting_desc')
        if(sortingASC) sortingASC.classList.remove('sorting_asc')
        if(sortingDESC) sortingDESC.classList.remove('sorting_desc')
        if(toggle === false){
            order = "asc"
            e.target.classList.add('sorting_asc')
            e.target.classList.remove('sorting_desc')
        }
        else {
            order = "desc"
            e.target.classList.remove('sorting_asc')
            e.target.classList.add('sorting_desc')
        }
        toggle = !toggle
        currentPage = 1
        showItemReload()
    })
})

function main() {
    try {
        getData(showItemReload)
    } catch (error) {
        alert("Loading failed")
    } finally {
    }
}

main()

