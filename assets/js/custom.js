const html = document.documentElement
const iconMenu = document.querySelector('.icon-menu')
const iconClose = document.querySelector('.icon-close')
const headerDropDown = document.querySelectorAll('.event-down')
const iconArrowDownList = document.querySelectorAll('.arrow-down')
const modal = document.querySelector('.modal')
const subnavList = document.querySelectorAll('.subnav-item .hidden')
const sections = document.querySelectorAll('section');
const textTyping = document.querySelector('.text-typing')
const inputCheck = document.querySelector("input[type=checkbox]")
const pricingAmount = document.querySelector('.pricing-amount')
const cardImg = document.querySelector('.card-img img')
const cardBodyCenter = document.querySelector('.testimonials .card-slider-center')
const cardBodyLeft = document.querySelector('.testimonials .card-slider-left')
const buttons = document.querySelectorAll('.testimonials button')
const countElements = document.querySelectorAll('.count');
const inputs = document.querySelectorAll('.form-input input')
const messInputs = document.querySelectorAll('.mess-input')
const label = document.querySelectorAll('.form-input label')
const form = document.querySelector('.about form')
let height = 100
let cardIndex = 1
let animationCountDone = false
let toggle = false;


// Open / close header
iconMenu.addEventListener('click', () => {
    modal.classList.add('show')
    document.body.style.overflowY = 'hidden'
    modal.style.marginRight = '17px'
})

iconClose.addEventListener('click', () => {
    modal.classList.remove('show')
    document.body.style.overflowY = 'scroll'
    modal.style.marginRight = '0'

})

headerDropDown.forEach((item, index) => {
    item.addEventListener('click', () => {
        const subnavShow = document.querySelector('.subnav-item .show')
        if(subnavShow != null) {
            if(subnavShow != subnavList[index]) subnavShow.classList.remove('show')
        }
        subnavList[index].classList.toggle("show")
        iconArrowDownList[index].classList.toggle('fa-chevron-down')
        iconArrowDownList[index].classList.toggle('fa-chevron-up')
    })
})

// count
function countUp(upto, num, countElement) {
    if (upto <= num) {
      countElement.innerHTML = upto;
      setTimeout(() => {
        countUp(++upto, num, countElement);
      }, 20);
    } 
  }

function countDown(downto, num, countElement) {
    if (downto >= num) {
        countElement.innerHTML = downto;
        setTimeout(() => {
            countDown(--downto, num, countElement);
        }, 20);
      } 
}

// animation scroll element
window.addEventListener('scroll', function() {
    sections.forEach((section, index) => {
        if(index != 0) {
            if(`${section.id}` === 'stats'){
                    const top = section.getBoundingClientRect().top
                    const bottom = section.getBoundingClientRect().bottom
                    if(top < this.window.innerHeight && bottom >= 0) {
                        if(animationCountDone === false){
                            countElements.forEach((countElement) => {
                                countUp(0, countElement.innerHTML, countElement);
                            });
                            animationCountDone = true
                        }
                    }
            }
            const animations = document.querySelectorAll(`#${section.id} .animation`)
            animations.forEach(animation => {
                const top = animation.getBoundingClientRect().top
                const bottom = animation.getBoundingClientRect().bottom
                if(top < this.window.innerHeight && bottom >= 0) {
                    animation.classList.add(`${animation.dataset.animation}`)
                }
            })
        }
    })
})


// typing
const textLoad = () => {
    setTimeout(() => {
        textTyping.textContent = "developers."
    }, 0)
    setTimeout(() => {
        textTyping.textContent = "designers."
    }, 2000)
    setTimeout(() => {
        textTyping.textContent = "founders."
    }, 4000)
}
textLoad()
setInterval(textLoad, 6000)

// // checkbox checked
inputCheck.addEventListener('change', () => {
    if(inputCheck.checked){
        countUp(pricingAmount.innerHTML, 49, pricingAmount);
    } else {
        countDown(49, 29, pricingAmount);
    }
})


// Slider
function toggleCard(e) {
    cardIndex = e.target.id === 'next' ? 1 : -1
    if (toggle === false) {
        console.log(cardBodyCenter.style.transform, cardBodyLeft.style.transform, 123);
        cardImg.src = './assets/img/photo-1.jpg'
        cardImg.classList.add('animation-slider')
        cardBodyLeft.style.opacity  = '1'
        cardBodyLeft.style.transform  = 'translateX(0)'
        cardBodyCenter.style.transform  = `translateX(${cardIndex * 100}%)`
        cardBodyCenter.style.opacity  = '0'
        console.log(cardBodyCenter.style.transform, cardBodyLeft.style.transform);
        setTimeout(() => {
            cardImg.classList.remove('animation-slider')
        }, 500)
    } else {
        console.log(cardBodyCenter.style.transform, cardBodyLeft.style.transform, 456);
        cardImg.src = './assets/img/photo-26.jpg'
        cardImg.classList.add('animation-slider')
        cardBodyCenter.style.opacity  = '1'
        cardBodyCenter.style.transform  = 'translateX(0)'
        cardBodyLeft.style.transform  = `translateX(${cardIndex * 100}%)`
        cardBodyLeft.style.opacity  = '0'
        setTimeout(() => {
            cardImg.classList.remove('animation-slider')
        }, 500)
        console.log(cardBodyCenter.style.transform, cardBodyLeft.style.transform);

    }
    toggle = !toggle;
}

buttons.forEach(button => {
    button.addEventListener('click', toggleCard)
})



// Validate input
const messEmpty = "Vui lòng nhập trường này"
const messEmail = "Email không hợp lệ"
const messPassword = "Vui lòng nhập password có độ dài > 8"
let isInputValid = false

const isEmptyInput = (value) => {
    if(value) return true
    else return false
}

const checkEmail = (email) => {
    let regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    if(email.match(regex)){
        return true
    }
    return false
}

const checkMinLength = (str, minlength) => {
    if(str.length < minlength) return false
    return true 
}

const validateInput = (input, index) => {
    if(!isEmptyInput(input.value)){
        messInputs[index].innerHTML = messEmpty
    } 
    else if(!checkEmail(input.value) && index === 1){
        messInputs[index].innerHTML = messEmail
    }
    else if(!checkMinLength(input.value, 8) && index === 2){
        messInputs[index].innerHTML = messPassword
    }
    else {
        messInputs[index].innerHTML = ""
        if(index === inputs.length - 1) isInputValid= true
    }
}

inputs.forEach((input, index) => {
    input.addEventListener("keypress", function(event){
        if (event.key === "Enter") {
            event.preventDefault();
            validateInput(input, index)
        }
    })
})


const handleSubmit = (e) => {
    e.preventDefault()
    inputs.forEach((input, index) => {
        validateInput(input, index)
    })
}


form.addEventListener('submit', handleSubmit)















